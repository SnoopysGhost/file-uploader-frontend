import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { catchError } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private http: HttpClient) {}

  upload(event: any) {
    const files: File[] = event.target.files;
    if (files.length > 0) {
      const formData = new FormData();

      for (const file of files) {
        console.log(file);
        formData.append('file', file);
      }
      console.log(formData);
      this.http.post('http://localhost:8080/fileUploads', formData).subscribe(
        () => {console.log('Files uploads')},
        (error) => {
          console.log(error);
        }
      );
    }
  }
}
